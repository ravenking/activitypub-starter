package uk.co.foundationsedge.activitypubstarterclient.application.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uk.co.foundationsedge.activitypubstarterclient.domain.key.KeyRepository;
import uk.co.foundationsedge.activitypubstarterclient.domain.user.User;
import uk.co.foundationsedge.activitypubstarterclient.domain.user.UserRepository;

import java.security.KeyPairGenerator;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class UserService {

  private final UserRepository userRepository;
  private final KeyRepository keyRepository;
  private final KeyPairGenerator keyPairGenerator;

  public User create(CreateUserRequest createUserRequest) {
    var id = UUID.randomUUID();
    var keypair = keyPairGenerator.generateKeyPair();
    var user = new User(id, createUserRequest.getUsername(), createUserRequest.getFirstName(), createUserRequest.getLastName());
    keyRepository.save(user.keyPrefix(), keypair);
    userRepository.save(user);
    return fetchBy(id);
  }

  public User fetchBy(UUID id) {
    var user = userRepository.findById(id).orElseThrow();
    var keypair = keyRepository.findByPrefix(user.keyPrefix());
    return new User(user, keypair);
  }

  public User fetchBy(String username) {
    var user = userRepository.findByUsername(username).orElseThrow();
    var keypair = keyRepository.findByPrefix(user.keyPrefix());
    return new User(user, keypair);
  }
}
