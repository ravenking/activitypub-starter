package uk.co.foundationsedge.activitypubstarterclient.domain.key;

import java.nio.file.Path;
import java.security.KeyPair;

public interface KeyRepository {

  Path save(String prefix, KeyPair keyPair);

  KeyPair findByPrefix(String prefix);
}
