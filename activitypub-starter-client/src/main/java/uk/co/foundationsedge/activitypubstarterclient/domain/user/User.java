package uk.co.foundationsedge.activitypubstarterclient.domain.user;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Transient;
import lombok.NoArgsConstructor;

import java.security.KeyPair;
import java.util.UUID;

import static java.lang.String.format;


@NoArgsConstructor
@Entity(name = "userz")
public class User {

  @Id
  private UUID id;
  private String username;
  private String firstName;
  private String lastName;
  @Transient
  private KeyPair keyPair;

  public User(UUID id, String username, String firstName, String lastName) {
    this.id = id;
    this.username = username;
    this.firstName = firstName;
    this.lastName = lastName;
    this.keyPair = null;
  }

  public User(User user, KeyPair keyPair) {
    this.id = user.id;
    this.username = user.username;
    this.firstName = user.firstName;
    this.lastName = user.lastName;
    this.keyPair = keyPair;
  }

  public String keyPrefix() {
    return this.id.toString();
  }

  public String preferredName() {
    return format("%s %s", firstName, lastName);
  }

  public KeyPair keypair() {
    return keyPair;
  }
}
