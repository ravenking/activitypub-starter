package uk.co.foundationsedge.activitypubstarterclient.interfaces.api;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import uk.co.foundationsedge.activitypubstarterclient.application.user.CreateUserRequest;
import uk.co.foundationsedge.activitypubstarterclient.application.user.UserService;
import uk.co.foundationsedge.activitypubstarterclient.domain.user.User;

@RequiredArgsConstructor
@RestController("/admin")
public class AdminController {

  private final UserService userService;

  @PostMapping("/users")
  public User createUser(@RequestBody CreateUserRequest createUserRequest) {
    return userService.create(createUserRequest);
  }

}
