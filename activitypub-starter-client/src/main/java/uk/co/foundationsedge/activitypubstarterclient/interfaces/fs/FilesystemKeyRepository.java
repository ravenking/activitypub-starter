package uk.co.foundationsedge.activitypubstarterclient.interfaces.fs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import uk.co.foundationsedge.activitypubstarterclient.domain.key.KeyRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import static java.lang.String.format;

@Component
public class FilesystemKeyRepository implements KeyRepository {

  private final Path keyPath;

  public FilesystemKeyRepository(@Value("${activitypub-client.key-path}") String keyPath) {
    this.keyPath = Paths.get(keyPath);
  }

  @Override
  public Path save(String prefix, KeyPair keyPair) {
    // Worst possible way to store sensitive keys :-)
    var pathWithPrefix = createIfNotExists(keyPath.resolve(prefix));
    try {
      Files.write(pathWithPrefix.resolve("id_rsa"), keyPair.getPrivate().getEncoded());
      Files.write(pathWithPrefix.resolve("id_rsa.pub"), keyPair.getPublic().getEncoded());
    } catch (IOException e) {
      throw new RuntimeException(format("Failed to store keypair for [%s]", prefix), e);
    }
    return pathWithPrefix;
  }


  @Override
  public KeyPair findByPrefix(String prefix) {
    var pathWithPrefix = createIfNotExists(keyPath.resolve(prefix));
    try {
      var publicPart = Files.readAllBytes(pathWithPrefix.resolve("id_rsa.pub"));
      var privatePart = Files.readAllBytes(pathWithPrefix.resolve("id_rsa"));

      X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(publicPart);
      PKCS8EncodedKeySpec privateSpec = new PKCS8EncodedKeySpec(privatePart);

      KeyFactory kf = KeyFactory.getInstance("RSA");
      PublicKey publicKey = kf.generatePublic(publicSpec);
      PrivateKey privateKey = kf.generatePrivate(privateSpec);

      return new KeyPair(publicKey, privateKey);
    } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
      throw new RuntimeException(e);
    }
  }

  private Path createIfNotExists(Path path) {
    try {
      return Files.createDirectories(path);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
