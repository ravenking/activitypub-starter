package uk.co.foundationsedge.activitypubstarterclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActivityPubStarterClientApplication {

  public static void main(String[] args) {
    SpringApplication.run(ActivityPubStarterClientApplication.class, args);
  }

}
