package uk.co.foundationsedge.activitypubstarterclient.application.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import uk.co.foundationsedge.activitypubserver.service.Outbox;

@Service
public class MessagingService {
  private final Logger LOG = LoggerFactory.getLogger(MessagingService.class);
  private final Outbox outbox;

  public MessagingService(Outbox outbox) {
    this.outbox = outbox;
  }

  public void send(String to, String message) {
    LOG.info("sending message [{}] to [{}]", message, to);
    outbox.sendMessage(to, message);
  }
}
