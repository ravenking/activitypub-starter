package uk.co.foundationsedge.activitypubstarterclient.interfaces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import uk.co.foundationsedge.activitypubstarterclient.application.service.MessagingService;

@RestController()
public class UserController {

  private final Logger LOG = LoggerFactory.getLogger(UserController.class);

  private final MessagingService messagingService;

  public UserController(MessagingService messagingService) {
    this.messagingService = messagingService;
  }

  @PostMapping("/users/{userId}/messages")
  public ResponseEntity<Void> createMessage(@PathVariable String userId) {
    LOG.info("Requesting message sent by user [{}]", userId);
    messagingService.send("raevn@fosstodon.org", "Hello, World!");
    return ResponseEntity.accepted().build();
  }

}
