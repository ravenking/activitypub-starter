package uk.co.foundationsedge.activitypubstarterclient.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import uk.co.foundationsedge.activitypubserver.domain.lib.User;
import uk.co.foundationsedge.activitypubserver.service.UserLookup;
import uk.co.foundationsedge.activitypubstarterclient.application.user.UserService;

import java.util.NoSuchElementException;
import java.util.Optional;

@RequiredArgsConstructor
@Configuration
public class UserConfiguration {

  private final UserService userService;

  @Bean
  public UserLookup actorProvider() {
    return (username) -> {
      try {
        var user = userService.fetchBy(username);
        return Optional.of(asActivityPubUser(user));
      } catch (NoSuchElementException e) {
        return Optional.empty();
      }
    };
  }

  private User asActivityPubUser(uk.co.foundationsedge.activitypubstarterclient.domain.user.User user) {
    return new User(user.preferredName(), user.keypair());
  }
}
