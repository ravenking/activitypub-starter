package uk.co.foundationsedge.activitypubstarterclient.application.user;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class CreateUserRequest {
  private final String username;
  private final String firstName;
  private final String lastName;
}
