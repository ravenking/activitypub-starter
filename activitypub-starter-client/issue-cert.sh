#!/bin/zsh

DOCKER_HOST="ssh://vultr-root" docker-compose -f docker-compose.yaml run --rm  certbot certonly --webroot --webroot-path /var/www/certbot/ -d activitypub.foundationsedge.co.uk
