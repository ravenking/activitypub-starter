#!/bin/zsh

./gradlew build
success=$?
[ $success -eq 0 ] && DOCKER_HOST="ssh://vultr-root" docker-compose -f docker-compose.yaml build && exit 0
[ $success -ne 0 ] && echo "failed to build" && exit 1