ActivityPub Starter Client
==

Development
--

You'll need to do the following:

- Create a remote host capable of being accessed from the internet
- Ensure both Docker & docker-compose are installed
- Create a DNS entry for the remote host
- Create an SSH entry in `~/.ssh/config` called `vultr-root` (if you want to use the project scripts)
- Run `./gradlew build`
- Run `./build.sh` (to build the local projects and install images on remote host)
- Run `./start.sh` (to spin up remote containers)
- Run `./issue-cert.sh` (to have certbot install `Letsencrypt` certs)
- Run `./stop.sh` (to stop at any time)