package uk.co.foundationsedge.activitypubserver.service;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatRuntimeException;

class AccountExtractorTest {

    AccountExtractor accountExtractor = new AccountExtractor();

    @Test
    void usernameFromAccountUri() {
        assertThat(accountExtractor.usernameFrom("acct:bob@subdomain.domain.com")).isEqualTo("bob");
    }

    @Test
    void usernameFromHttpRef() {
        assertThat(accountExtractor.usernameFrom("http:bob@subdomain.domain.com")).isEqualTo("bob");
    }

    @Test
    void usernameFromHttpsRef() {
        assertThat(accountExtractor.usernameFrom("https:bob@subdomain.domain.com")).isEqualTo("bob");
    }

    @Test
    void usernameFromMailtoRef() {
        assertThat(accountExtractor.usernameFrom("mailto:bob@subdomain.domain.com")).isEqualTo("bob");
    }

    @Test
    void throwsExceptionWhenReferenceEmpty() {
        assertThatRuntimeException().isThrownBy(() -> accountExtractor.usernameFrom(""));
    }

    @Test
    void throwsExceptionWhenReferenceNull() {
        assertThatRuntimeException().isThrownBy(() -> accountExtractor.usernameFrom(null));
    }

    @Test
    void throwsExceptionWhenProtocolUnknown() {
        assertThatRuntimeException().isThrownBy(() -> accountExtractor.usernameFrom("myprotocol:bob@subdomain.domain.com"));
    }

    @Test
    void throwsExceptionWhenMissingAt() {
        assertThatRuntimeException().isThrownBy(() -> accountExtractor.usernameFrom("acct:bob"));
    }

    @Test
    void throwsExceptionWhenMissingDomain() {
        assertThatRuntimeException().isThrownBy(() -> accountExtractor.usernameFrom("acct:bob@"));
    }
}