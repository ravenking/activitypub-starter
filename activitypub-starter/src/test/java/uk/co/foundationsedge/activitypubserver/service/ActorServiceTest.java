package uk.co.foundationsedge.activitypubserver.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriBuilderFactory;
import uk.co.foundationsedge.activitypubserver.domain.lib.User;

import java.net.URI;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ActorServiceTest {

  @Mock
  UriBuilderFactory uriBuilderFactory;
  @Mock
  UserLookup userLookup;

  ActorService underTest;

  @BeforeEach
  public void beforeEach() {
    underTest = new ActorService(uriBuilderFactory, userLookup);

    var uriBuilder = new DefaultUriBuilderFactory(format("%s://%s", "https", "activitypub.foundationsedge.co.uk")).builder();

    when(this.uriBuilderFactory.builder()).thenReturn(uriBuilder);
    when(this.userLookup.forUsername("raevn")).thenReturn(Optional.of(new User("raevn", keyPair())));
  }

  @Test
  public void actorResponseConformWithActivityStreamsProtocol() {
    var actor = underTest.actor("raevn");

    assertThat(actor.getId()).isEqualTo(URI.create("https://activitypub.foundationsedge.co.uk/actors/raevn"));
    assertThat(actor.getType()).isEqualTo("Person");
  }

  private KeyPair keyPair() {
    try {
      KeyPairGenerator keypairGenerator = KeyPairGenerator.getInstance("RSA");
      keypairGenerator.initialize(2048);
      return keypairGenerator.generateKeyPair();
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }
}