package uk.co.foundationsedge.activitypubserver.domain.lib;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.security.KeyPair;

@RequiredArgsConstructor
@Getter
public class User {
  private final String preferredName;
  private final KeyPair keypair;
}
