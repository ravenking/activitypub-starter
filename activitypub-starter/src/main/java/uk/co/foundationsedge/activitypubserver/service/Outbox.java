package uk.co.foundationsedge.activitypubserver.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;

import static java.lang.String.format;

public class Outbox {

  private final RabbitTemplate rabbitTemplate;

  public Outbox(RabbitTemplate rabbitTemplate) {
    this.rabbitTemplate = rabbitTemplate;
  }

  public void sendMessage(String to, String message) {
    rabbitTemplate.convertAndSend("avtivitypub", format("outbox.%s", to), message);
  }
}
