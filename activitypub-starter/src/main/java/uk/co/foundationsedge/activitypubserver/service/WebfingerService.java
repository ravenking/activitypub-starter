package uk.co.foundationsedge.activitypubserver.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriBuilderFactory;
import uk.co.foundationsedge.activitypubserver.domain.webfinger.Link;
import uk.co.foundationsedge.activitypubserver.domain.webfinger.Webfinger;

import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static java.lang.String.format;

public class WebfingerService {
  private final Logger LOG = LoggerFactory.getLogger(WebfingerService.class);

  private final AccountExtractor accountExtractor;
  private final UriBuilderFactory uriBuilderFactory;
  private final UserLookup userLookup;


  public WebfingerService(AccountExtractor accountExtractor,
                          UriBuilderFactory uriBuilderFactory,
                          UserLookup userLookup) {
    this.accountExtractor = accountExtractor;
    this.uriBuilderFactory = uriBuilderFactory;
    this.userLookup = userLookup;
  }

  public Webfinger finger(String resource) {
    var decodedResource = URLDecoder.decode(resource, StandardCharsets.UTF_8);
    var uriBuilder = uriBuilderFactory.builder();
    LOG.info("inbound webfinger request [{}]", decodedResource);
    LOG.info("domainUri [{}]", uriBuilder.build());
    var username = accountExtractor.usernameFrom(decodedResource);

    // TODO Lookup user to make sure they exits, don't need object otherwise
    var user = userLookup.forUsername(username).orElseThrow(() -> new RuntimeException(format("can't find actor given resource %s", username)));

    return new Webfinger(
        decodedResource,
        null,
        null,
        List.of(
            new Link(
                "self",
                "application/activity+json",
                URI.create("https://activitypub.foundationsedge.co.uk/actors/" + username)
//                domainUri.resolve(domainUri.getPath() + format("/actors/%s", username)
            )
        )
    );

  }
}
