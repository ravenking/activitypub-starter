package uk.co.foundationsedge.activitypubserver.service;


import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriBuilderFactory;
import uk.co.foundationsedge.activitypubserver.domain.activitystream.actor.Actor;
import uk.co.foundationsedge.activitypubserver.domain.activitystream.core.OrderedCollection;

import static java.lang.String.format;
import static uk.co.foundationsedge.activitypubserver.domain.activitystream.core.OrderedCollection.orderedCollection;

public class ActorService {
  private final Logger LOG = LoggerFactory.getLogger(ActorService.class);
  private final UriBuilderFactory uriBuilderFactory;
  private final UserLookup userLookup;

  public ActorService(UriBuilderFactory uriBuilderFactory,
                      UserLookup userLookup) {
    this.uriBuilderFactory = uriBuilderFactory;
    this.userLookup = userLookup;
  }

  public Actor actor(String actor) {
    LOG.info("Request for actor [{}]", actor);
    var user = userLookup.forUsername(actor).orElseThrow(() -> new RuntimeException(format("can't find actor given resource %s", actor)));
    var domainUri = uriBuilderFactory.builder().build();
    return Actor.person(domainUri)
        .withPublicKey(user.getKeypair().getPublic())
        .buildUser(actor);
  }

  public OrderedCollection outboxFor(String actor) {
//    var user = userLookup.forUsername(actor);
    var uriBuilder = uriBuilderFactory.builder();
    var id = uriBuilder.path(format("actors/%s/outbox", actor)).build();
    var first = uriBuilder.path(format("actors/%s/outbox", actor)).queryParam("page", true).build();
    var last = uriBuilder.path(format("actors/%s/outbox", actor)).queryParam("min_id", 0).queryParam("page", true).build();
    return orderedCollection(
        id,
        0L,
        first,
        last
    );
  }

  public OrderedCollection followingFor(String actor) {
//    var user = userLookup.forUsername(actor);
    var uriBuilder = uriBuilderFactory.builder();
    var id = uriBuilder.path(format("actors/%s/following", actor)).build();
    var first = uriBuilder.path(format("actors/%s/following", actor)).queryParam("page", true).build();
    var last = uriBuilder.path(format("actors/%s/following", actor)).queryParam("min_id", 0).queryParam("page", true).build();
    return orderedCollection(
        id,
        0L,
        first,
        last
    );
  }

  public OrderedCollection followersFor(String actor) {
//    var user = userLookup.forUsername(actor);
    var uriBuilder = uriBuilderFactory.builder();
    var id = uriBuilder.path(format("actors/%s/followers", actor)).build();
    var first = uriBuilder.path(format("actors/%s/followers", actor)).queryParam("page", true).build();
    var last = uriBuilder.path(format("actors/%s/followers", actor)).queryParam("min_id", 0).queryParam("page", true).build();
    return orderedCollection(
        id,
        0L,
        first,
        last
    );
  }

  public void toInbox(String actor, JsonNode jsonNode, String signature, String digest) {
    LOG.info("actor [{}] had a new inbox message [{}] signed [{}] digest [{}]", actor, jsonNode, signature, digest);
  }

  public void sendMessage(String to, String message) {

  }
}
