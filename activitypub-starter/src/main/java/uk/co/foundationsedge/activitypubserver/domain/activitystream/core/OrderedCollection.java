package uk.co.foundationsedge.activitypubserver.domain.activitystream.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.net.URI;

@JsonPropertyOrder({"@context", "id", "type", "totalItems", "first", "last"})
public record OrderedCollection(@JsonProperty("@context") URI context,
                                URI id,
                                String type,
                                Long totalItems,
                                URI first,
                                URI last) {
  public static OrderedCollection orderedCollection(URI id,
                                                    Long totalItems,
                                                    URI first,
                                                    URI last) {
    return new OrderedCollection(
        URI.create("https://www.w3.org/ns/activitystreams"),
        id,
        "OrderedCollection",
        totalItems,
        first,
        last
    );
  }
}
