package uk.co.foundationsedge.activitypubserver.service;

import uk.co.foundationsedge.activitypubserver.domain.lib.User;

import java.util.Optional;

/**
 * Interface used by clients to hook into ActivityPub starter.
 * <p>
 * Intended to allow the library to make use of existing authentication in client app.
 */
@FunctionalInterface
public interface UserLookup {
  Optional<User> forUsername(String username);
}
