package uk.co.foundationsedge.activitypubserver.domain.webfinger;

import java.net.URI;

public record Link(String rel, String type, URI href) {
}
