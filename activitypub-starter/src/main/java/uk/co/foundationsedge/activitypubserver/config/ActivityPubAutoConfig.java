package uk.co.foundationsedge.activitypubserver.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriBuilderFactory;
import uk.co.foundationsedge.activitypubserver.application.ActivityPubController;
import uk.co.foundationsedge.activitypubserver.infrastructure.rabbitmq.Receiver;
import uk.co.foundationsedge.activitypubserver.service.*;

import static java.lang.String.format;

@Configuration
@ConditionalOnProperty(value = "activitypub.enabled", havingValue = "true")
@EnableConfigurationProperties(ActivityPubProperties.class)
public class ActivityPubAutoConfig {

  @Bean
  public AccountExtractor accountExtractor() {
    return new AccountExtractor();
  }

  @Bean
  public UriBuilderFactory uriBuilderFactory(ActivityPubProperties activityPubProperties) {
    return new DefaultUriBuilderFactory
        (format("%s://%s", activityPubProperties.getScheme(), activityPubProperties.getDomain()));
  }

  @Bean
  public WebfingerService webfingerService(AccountExtractor accountExtractor,
                                           UriBuilderFactory uriBuilderFactory,
                                           UserLookup userLookup) {
    return new WebfingerService(
        accountExtractor,
        uriBuilderFactory,
        userLookup);
  }

  @Bean
  public ActorService actorService(UriBuilderFactory uriBuilderFactory,
                                   UserLookup userLookup) {
    return new ActorService(uriBuilderFactory, userLookup);
  }

  @Bean
  public ActivityPubController activityPub(WebfingerService webfingerService,
                                           ActorService actorService) {
    return new ActivityPubController(webfingerService, actorService);
  }

  @Bean
  Queue inboxQueue() {
    return new Queue("inbox", false);
  }

  @Bean
  Queue outboxQueue() {
    return new Queue("outbox", false);
  }

  @Bean
  TopicExchange topicExchange() {
    return new TopicExchange("avtivitypub");
  }

  @Bean
  Binding outboxBinding(Queue outboxQueue,
                        TopicExchange topicExchange) {
    return BindingBuilder.bind(outboxQueue).to(topicExchange).with("outbox.#");
  }

  @Bean
  Binding inboxBinding(Queue inboxQueue,
                       TopicExchange topicExchange) {
    return BindingBuilder.bind(inboxQueue).to(topicExchange).with("inbox.#");
  }

  @Bean
  CachingConnectionFactory connectionFactory(@Value("${activitypub.rabbitmq.host}") String hostname,
                                             @Value("${activitypub.rabbitmq.username}") String username,
                                             @Value("${activitypub.rabbitmq.passwd}") String password) {
    var connectionFactory = new CachingConnectionFactory(hostname);
    connectionFactory.setUsername(username);
    connectionFactory.setPassword(password);
    return connectionFactory;
  }

  @Bean
  SimpleMessageListenerContainer inboxListeningContainer(ConnectionFactory connectionFactory,
                                                         MessageListener inboundMessageListener) {
    SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
    container.setConnectionFactory(connectionFactory);
    container.setQueueNames("inbox");
    container.setMessageListener(inboundMessageListener);
    return container;
  }

  @Bean
  SimpleMessageListenerContainer outboxListeningContainer(ConnectionFactory connectionFactory,
                                                          MessageListener inboundMessageListener) {
    SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
    container.setConnectionFactory(connectionFactory);
    container.setQueueNames("outbox");
    container.setMessageListener(inboundMessageListener);
    return container;
  }

  @Bean
  public MessageListener inboundMessageListener(Receiver receiver) {
    return receiver::receiveMessage;
  }


  @Bean
  Receiver receiver() {
    return new Receiver();
  }

  @Bean
  Outbox outbox(RabbitTemplate rabbitTemplate) {
    return new Outbox(rabbitTemplate);
  }
}
