package uk.co.foundationsedge.activitypubserver.domain.activitystream.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import uk.co.foundationsedge.activitypubserver.domain.activitystream.Either;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Activity extends Obj {
  private Either actor;
  private Obj object;
  private Either target;
  private Either result;
  private Either origin;
  private Either instrument;


  public static class AcceptBuilder {
    private final Either<Link, Obj> context;
    private final String summary;
    private Either<Link, Obj> actor;
    private Obj object;
    private Either<Link, Obj> target;

    public AcceptBuilder(Either<Link, Obj> context, String summary) {
      this.context = context;
      this.summary = summary;
    }

    public AcceptBuilder withActor(Either<Link, Obj> actor) {
      this.actor = actor;
      return this;
    }

    public AcceptBuilder withObject(Obj object) {
      this.object = object;
      return this;
    }

    public AcceptBuilder withTarget(Either<Link, Obj> target) {
      this.target = target;
      return this;
    }

    public Activity build() {
      var activity = new Activity();
      activity.type = "Accept";
      activity.context = this.context;
      activity.actor = this.actor;
      activity.object = this.object;
      activity.target = this.target;
      return activity;
    }
  }

  public static class CreateBuilder {
    private final Either<Link, Obj> context;
    private final String summary;
    private Either<Link, Obj> actor;
    private Obj object;
    private Either<Link, Obj> target;

    public CreateBuilder(Either<Link, Obj> context, String summary) {
      this.context = context;
      this.summary = summary;
    }

    public CreateBuilder withActor(Either<Link, Obj> actor) {
      this.actor = actor;
      return this;
    }

    public CreateBuilder withObject(Obj object) {
      this.object = object;
      return this;
    }

    public CreateBuilder withTarget(Either<Link, Obj> target) {
      this.target = target;
      return this;
    }

    public Activity build() {
      var activity = new Activity();
      activity.type = "Create";
      activity.context = this.context;
      activity.actor = this.actor;
      activity.object = this.object;
      activity.target = this.target;
      return activity;
    }
  }

  public static class DeleteBuilder {
    private final Either<Link, Obj> context;
    private final String summary;
    private Either<Link, Obj> actor;
    private Obj object;
    private Either<Link, Obj> origin;

    public DeleteBuilder(Either<Link, Obj> context, String summary) {
      this.context = context;
      this.summary = summary;
    }

    public DeleteBuilder withActor(Either<Link, Obj> actor) {
      this.actor = actor;
      return this;
    }

    public DeleteBuilder withObject(Obj object) {
      this.object = object;
      return this;
    }

    public DeleteBuilder withOrigin(Either<Link, Obj> target) {
      this.origin = origin;
      return this;
    }

    public Activity build() {
      var activity = new Activity();
      activity.type = "Delete";
      activity.context = this.context;
      activity.actor = this.actor;
      activity.object = this.object;
      activity.origin = this.origin;
      return activity;
    }
  }


}
