package uk.co.foundationsedge.activitypubserver.service;

import java.util.regex.Pattern;

import static java.lang.String.format;

public class AccountExtractor {

    private final Pattern resourcePattern;

    public AccountExtractor() {
        this.resourcePattern = Pattern.compile("^(acct|mailto|http[s]?):([a-zA-Z0-9]{1,})@([A-Za-z0-9](?:(?:[-A-Za-z0-9]){0,61}[A-Za-z0-9])?(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)+)$");
    }

    public String usernameFrom(String resource) {
        var matcher = resourcePattern.matcher(resource);
        if (!matcher.matches())
            throw new RuntimeException(format("Couldn't find username given reference %s", resource));
        return matcher.group(2);
    }
}
