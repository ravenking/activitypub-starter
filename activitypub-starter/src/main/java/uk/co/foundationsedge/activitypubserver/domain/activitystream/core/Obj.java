package uk.co.foundationsedge.activitypubserver.domain.activitystream.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import uk.co.foundationsedge.activitypubserver.domain.activitystream.Either;
import uk.co.foundationsedge.activitypubserver.domain.activitystream.object.Image;
import uk.co.foundationsedge.activitypubserver.domain.properties.Duration;

import java.net.URI;
import java.util.Date;


@JsonPropertyOrder({
    "@context", "type", "attachment", "attributedTo", "audience", "content", "context", "name", "endTime", "generator",
    "icon", "image", "inReplyTo", "location", "preview", "published", "replies", "startTime", "summary", "tag",
    "updated", "url", "to", "bto", "cc", "bcc", "mediaType", "duration", "altitude", "radius"})
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Obj {
  protected String type;
  protected String id;
  protected Either<Link, Obj> context;
  protected String name;
  protected Either<Link, Obj> attachment;
  protected Either<Link, Obj> attributedTo;
  protected Either<Link, Obj> audience;
  protected String content;
  protected Date endTime;
  protected Either<Link, Obj> generator;
  protected Either<Link, Image> icon;
  protected Either<Link, Image> image;
  protected Either<Link, Obj> inReplyTo;
  protected Either<Link, Obj> location;
  protected Either<Link, Obj> preview;
  protected Date published;
  protected Collection replies;
  protected Date startTime;
  protected String summary;
  protected Either<Link, Obj> tag;
  protected Date updated;
  protected URI url;
  protected Either<Link, Obj> to;
  protected Either<Link, Obj> bto;
  protected Either<Link, Obj> cc;
  protected Either<Link, Obj> bcc;
  protected String mediaType;
  protected Duration duration;
  protected Long altitude;
  protected Float radius;

  public Builder builder(Either context, String type) {
    return new Builder(context, type);
  }

  private static class Builder {
    private final String type;
    private final Either<Link, Obj> context;
    private String id;
    private String name;
    private Either<Link, Obj> attachment;
    private Either<Link, Obj> attributedTo;
    private Either<Link, Obj> audience;
    private String content;
    private Date endTime;
    private Either<Link, Obj> generator;
    private Either<Link, Image> icon;
    private Either<Link, Image> image;
    private Either<Link, Obj> inReplyTo;
    private Either<Link, Obj> location;
    private Either<Link, Obj> preview;
    private Date published;
    private Collection replies;
    private Date startTime;
    private String summary;
    private Either<Link, Obj> tag;
    private Date updated;
    private URI url;
    private Either<Link, Obj> to;
    private Either<Link, Obj> bto;
    private Either<Link, Obj> cc;
    private Either<Link, Obj> bcc;
    private String mediaType;
    private Duration duration;
    private Long altitude;
    private Float radius;

    public Builder(Either<Link, Obj> context, String type) {
      this.context = context;
      this.type = type;
    }

    public Builder withId(String id) {
      this.id = id;
      return this;
    }

    public Builder withName(String name) {
      this.name = name;
      return this;
    }

    public Builder withAttachment(Either<Link, Obj> attachment) {
      this.attachment = attachment;
      return this;
    }

    public Builder withAttributedTo(Either<Link, Obj> attributedTo) {
      this.attributedTo = attributedTo;
      return this;
    }

    public Builder withAudience(Either<Link, Obj> audience) {
      this.audience = audience;
      return this;
    }

    public Builder withContent(String content) {
      this.content = content;
      return this;
    }

    public Builder withEndTime(Date endTime) {
      this.endTime = endTime;
      return this;
    }

    public Builder withGenerator(Either<Link, Obj> generator) {
      this.generator = generator;
      return this;
    }

    public Builder withIcon(Either<Link, Image> icon) {
      this.icon = icon;
      return this;
    }

    public Builder withImage(Either<Link, Image> image) {
      this.image = image;
      return this;
    }

    public Builder withInReplyTo(Either<Link, Obj> inReplyTo) {
      this.inReplyTo = inReplyTo;
      return this;
    }

    public Builder withLocation(Either<Link, Obj> location) {
      this.location = location;
      return this;
    }

    public Builder withPreview(Either<Link, Obj> preview) {
      this.preview = preview;
      return this;
    }

    public Builder withPublished(Date published) {
      this.published = published;
      return this;
    }

    public Builder withReplies(Collection replies) {
      this.replies = replies;
      return this;
    }

    public Builder withStartTime(Date startTime) {
      this.startTime = startTime;
      return this;
    }

    public Builder withSummary(String summary) {
      this.summary = summary;
      return this;
    }

    public Builder withTag(Either<Link, Obj> tag) {
      this.tag = tag;
      return this;
    }

    public Builder withUpdated(Date updated) {
      this.updated = updated;
      return this;
    }

    public Builder withUrl(URI url) {
      this.url = url;
      return this;
    }

    public Builder withTo(Either<Link, Obj> to) {
      this.to = to;
      return this;
    }

    public Builder withBto(Either<Link, Obj> bto) {
      this.bto = bto;
      return this;
    }

    public Builder withCc(Either<Link, Obj> cc) {
      this.cc = cc;
      return this;
    }

    public Builder withBcc(Either<Link, Obj> bcc) {
      this.bcc = bcc;
      return this;
    }

    public Builder withMediaType(String mediaType) {
      this.mediaType = mediaType;
      return this;
    }

    public Builder withDuration(Duration duration) {
      this.duration = duration;
      return this;
    }

    public Builder withAltitude(Long altitude) {
      this.altitude = altitude;
      return this;
    }

    public Builder withRadius(Float radius) {
      this.radius = radius;
      return this;
    }

    public Obj build() {
      return new Obj(
          type,
          id,
          context,
          name,
          attachment,
          attributedTo,
          audience,
          content,
          endTime,
          generator,
          icon,
          image,
          inReplyTo,
          location,
          preview,
          published,
          replies,
          startTime,
          summary,
          tag,
          updated,
          url,
          to,
          bto,
          cc,
          bcc,
          mediaType,
          duration,
          altitude,
          radius
      );
    }
  }
}
