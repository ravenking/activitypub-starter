package uk.co.foundationsedge.activitypubserver.domain.activitystream.actor;

import lombok.Getter;

@Getter
public class Person extends Actor {
//  private final Optional<String> preferredUsername;
//  private final URI inbox;
//  private final URI outbox;
//  private final URI following;
//  private final URI followers;
//  private final URI liked;
//  private final PublicKey publicKey;
//
//  public Person(URI id, String type, Optional<String> preferredUsername, URI inbox, URI outbox, URI following, URI followers, URI liked, PublicKey publicKey) {
//    super(id, type);
//    this.preferredUsername = preferredUsername;
//    this.inbox = inbox;
//    this.outbox = outbox;
//    this.following = following;
//    this.followers = followers;
//    this.liked = liked;
//    this.publicKey = publicKey;
//  }
//
//  public static class Builder extends Actor.Builder {
//    private String publicKeyPem;
//
//    public Builder(URI domain) {
//      super(domain);
//    }
//
//    public Builder withPublicKey(java.security.PublicKey publicKey) {
//      StringWriter stringWriter = new StringWriter();
//      JcaPEMWriter pemWriter = new JcaPEMWriter(stringWriter);
//      try {
//        pemWriter.writeObject(publicKey);
//        pemWriter.close();
//        this.publicKeyPem = stringWriter.toString();
//        return this;
//      } catch (IOException e) {
//        throw new RuntimeException(e);
//      }
//    }
//
//    public Actor buildUser(String username) {
//      this.withType("Person");
//
//      var actor = URI.create(format("%s/actors/%s", domain, username));
//      var inbox = URI.create(format("%s/inbox", actor));
//      var outbox = URI.create(format("%s/outbox", actor));
//      var following = URI.create(format("%s/following", actor));
//      var followers = URI.create(format("%s/followers", actor));
//      var liked = URI.create(format("%s/liked", actor));
//
//
//      var actorFragment = actor.resolve("#main-key");
//      var publicKey = PublicKey.of(actorFragment, actor, publicKeyPem);
//
//      return new Person(
//          actor,
//          type,
//          Optional.of(username),
//          inbox,
//          outbox,
//          following,
//          followers,
//          liked,
//          publicKey);
//    }
//  }
}
