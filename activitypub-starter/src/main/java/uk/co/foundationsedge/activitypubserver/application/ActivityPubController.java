package uk.co.foundationsedge.activitypubserver.application;

import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uk.co.foundationsedge.activitypubserver.domain.activitystream.actor.Actor;
import uk.co.foundationsedge.activitypubserver.domain.activitystream.core.OrderedCollection;
import uk.co.foundationsedge.activitypubserver.domain.webfinger.Webfinger;
import uk.co.foundationsedge.activitypubserver.service.ActorService;
import uk.co.foundationsedge.activitypubserver.service.WebfingerService;

@RestController
public class ActivityPubController {

  private final Logger LOG = LoggerFactory.getLogger(ActivityPubController.class);

  private final WebfingerService webfingerService;
  private final ActorService actorService;

  public ActivityPubController(WebfingerService webfingerService, ActorService actorService) {
    this.webfingerService = webfingerService;
    this.actorService = actorService;
  }

  @GetMapping(value = "/.well-known/webfinger", produces = "application/jrd+json")
  public Webfinger webFinger(@RequestParam("resource") String resource) {
    return webfingerService.finger(resource);
  }

  @GetMapping(value = "/actors/{actor}", produces = "application/activity+json")
  public Actor actor(@PathVariable("actor") String actor) {
    return actorService.actor(actor);
  }

  @GetMapping(value = "/actors/{actor}/outbox", produces = "application/activity+json")
  public OrderedCollection outbox(@PathVariable("actor") String actor) {
    return actorService.outboxFor(actor);
  }

  @GetMapping(value = "/actors/{actor}/following", produces = "application/activity+json")
  public OrderedCollection following(@PathVariable("actor") String actor) {
    return actorService.followingFor(actor);
  }

  @GetMapping(value = "/actors/{actor}/followers", produces = "application/activity+json")
  public OrderedCollection followers(@PathVariable("actor") String actor) {
    return actorService.followersFor(actor);
  }

  @PostMapping(value = "/actors/{actor}/inbox", consumes = "application/activity+json", produces = "application/activity+json")
  public ResponseEntity<Void> inbox(@PathVariable("actor") String actor,
                                    @RequestHeader("signature") String signature,
                                    @RequestHeader("digest") String digest,
                                    @RequestBody JsonNode jsonNode) {
    actorService.toInbox(actor, jsonNode, signature, digest);
    return ResponseEntity.accepted().build();
  }
}
