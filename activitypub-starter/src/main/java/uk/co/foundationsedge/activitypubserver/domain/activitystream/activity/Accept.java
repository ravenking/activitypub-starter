package uk.co.foundationsedge.activitypubserver.domain.activitystream.activity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import uk.co.foundationsedge.activitypubserver.domain.activitystream.core.Activity;

@Getter
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Accept extends Activity {

  public Builder builder() {
    return new Builder();
  }


}