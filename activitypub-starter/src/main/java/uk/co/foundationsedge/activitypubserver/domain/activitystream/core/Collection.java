package uk.co.foundationsedge.activitypubserver.domain.activitystream.core;

import uk.co.foundationsedge.activitypubserver.domain.activitystream.Either;

import java.util.List;

public class Collection {
  private List<Either<Link, Obj>> items;
  private Integer totalItems;

  public Either<Link, CollectionPage> getFirst() {
    if (!items.isEmpty()) {
      return Either.left(items.getFirst().getLeft()); // TODO: not sure how CollectionPage works
    }
    return null;
  }

  public Either<Link, CollectionPage> getLast() {
    if (!items.isEmpty()) {
      return Either.left(items.getLast().getLeft()); // TODO: not sure how CollectionPage works
    }
    return null;
  }

  public Either<Link, CollectionPage> getCurrent() {
    // See https://www.w3.org/TR/activitystreams-core/#dfn-collectionpage
    return null;
  }
}
