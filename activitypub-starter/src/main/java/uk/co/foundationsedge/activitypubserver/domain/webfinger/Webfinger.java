package uk.co.foundationsedge.activitypubserver.domain.webfinger;

import java.net.URI;
import java.util.List;
import java.util.Map;

public record Webfinger(String subject,
                        List<URI> aliases,
                        Map<URI, String> properties,
                        List<Link> links) {
}
