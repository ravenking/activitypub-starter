package uk.co.foundationsedge.activitypubserver.application.dto;

import lombok.Getter;

import java.net.URI;

@Getter
public final class PublicKey {
  private final URI id;
  private final URI owner;
  private final String publicKeyPem;

  public PublicKey(URI id, URI owner, String publicKeyPem) {
    this.id = id;
    this.owner = owner;
    this.publicKeyPem = publicKeyPem;
  }

  public static PublicKey of(URI id, URI owner, String publicKeyPem) {
    return new PublicKey(id, owner, publicKeyPem);
  }
}
