package uk.co.foundationsedge.activitypubserver.domain.activitystream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Either<L, R> {
  private final L left;
  private final R right;

  @JsonCreator
  private Either(@JsonProperty("left") L left, @JsonProperty("right") R right) {
    this.left = left;
    this.right = right;
  }

  public static <L, R> Either<L, R> left(L left) {
    return new Either<>(left, null);
  }

  public static <L, R> Either<L, R> right(R right) {
    return new Either<>(null, right);
  }

  @JsonIgnore
  public boolean isLeft() {
    return left != null;
  }

  @JsonIgnore
  public boolean isRight() {
    return right != null;
  }

  public L getLeft() {
    return left;
  }

  public R getRight() {
    return right;
  }
}
