package uk.co.foundationsedge.activitypubserver.domain.activitystream.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.MediaType;
import uk.co.foundationsedge.activitypubserver.domain.activitystream.Either;

import java.net.URI;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Link {
  private String type;
  private URI href;
  private Either context;
  private String hreflang; // TODO: See https://www.rfc-editor.org/rfc/rfc5646.txt
  private MediaType mediaType;
  private String name;
  private String rel; // TODO: See https://datatracker.ietf.org/doc/html/rfc5988
  private Integer height; // TODO: Should be a positive value
  private Integer width; // TODO: Should be a positive value
  private Either preview;

  public static Builder builder(String type, URI href) {
    return new Builder(type, href);
  }

  public static class Builder {
    private final String type;
    private final URI href;
    private Either context;
    private String hreflang;
    private MediaType mediaType;
    private String name;
    private String rel;
    private Integer height;
    private Integer width;
    private Either preview;

    private Builder(String type, URI href) {
      this.type = type;
      this.href = href;
    }

    public Builder context(Either context) {
      this.context = context;
      return this;
    }

    public Builder hreflang(String hreflang) {
      this.hreflang = hreflang;
      return this;
    }

    public Builder mediaType(MediaType mediaType) {
      this.mediaType = mediaType;
      return this;
    }

    public Builder name(String name) {
      this.name = name;
      return this;
    }

    public Builder rel(String rel) {
      this.rel = rel;
      return this;
    }

    public Builder height(Integer height) {
      this.height = height;
      return this;
    }

    public Builder width(Integer width) {
      this.width = width;
      return this;
    }

    public Builder preview(Either preview) {
      this.preview = preview;
      return this;
    }

    public Link build() {
      return new Link(type, href, context, hreflang, mediaType, name, rel, height, width, preview);
    }
  }
}