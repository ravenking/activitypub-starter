package uk.co.foundationsedge.activitypubserver.config;

import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "activitypub.server")
@ConditionalOnProperty(value = "activitypub.enabled", havingValue = "true")
public class ActivityPubProperties {
  private final String domain;
  private final String scheme;
}
