package uk.co.foundationsedge.activitypubserver.infrastructure.rabbitmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;

public class Receiver {

  private final Logger LOG = LoggerFactory.getLogger(Receiver.class);

  public void receiveMessage(Message message) {
//    var to = StringUtils.substringAfter(message.getMessageProperties().getReceivedRoutingKey(), ".");
//    var message = create("A test note from raevn@activitypub.foundationsedge.co.uk", person(), note())
//    LOG.info("Received message from the Q [{}] body [{}]", message, new String(message.getBody()));
  }
}
